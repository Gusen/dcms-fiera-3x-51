<?php

if (isset($_POST['close'])) {
    $reason = mysql_real_escape_string(trim($_POST['reason']));
    $cause = ($reason == NULL) ? ' неизвестна.' : ': '.output_text($reason);
    $a = ($user['pol'] == 0) ? 'a' : NULL;
    $reason = 'Тему закрыл'.$a.' '.$user['nick'].'<br />Причина'.$cause;
    if (mb_strlen($reason) > 100) {
        ?>
        <div class = 'err'>Слишком длинная причина. Максимум 100 символов.</div>
        <?
    } else {
        $msg_sys = '[url=/info.php?id='.$user['id'].']'.$user['nick'].'[/url] закрыл тему. Причина указана выше.';
        mysql_query('INSERT INTO `forum_posts` SET `id_theme` = '.$theme->id.', `id_user` = "0", `id_admin` = "0", `text` = "'.mysql_real_escape_string($msg_sys).'", `time` = '.$time);
        mysql_query('UPDATE `forum_themes` SET `reason_close` = "'.mysql_real_escape_string(trim($reason)).'" WHERE `id` = '.$theme->id);
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        exit;
    }
} else {
    ?>
    <div class = 'menu_razd'>Закрытие темы "<?= output_text($theme->name, 1, 1, 0, 0, 0) ?>"</div>
    <form action = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/close_theme.html' method = 'post' class="p_m">
        <b>Укажите причину:</b><br />
        <input type = 'text' name = 'reason' value = '' style = 'width: 92%' /><br />
        <input type = 'submit' name = 'close' value = 'Закрыть тему' />
    </form>
    <div class = 'p_m' style = 'text-align: right'><a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'>Отменить закрытие</a></div>
    <?
    include_once '../sys/inc/tfoot.php';
    exit;
}

?>